consult-el (1.8-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * d/control: Bump Standards-Version to 4.7.0 (no changes required).

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Tue, 29 Oct 2024 22:39:36 +0100

consult-el (1.7-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 17:51:21 +0900

consult-el (1.7-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 24 Jul 2024 21:35:47 +0900

consult-el (1.7-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Tue, 23 Jul 2024 04:43:35 +0200

consult-el (1.4-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Bump copyright years.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Sat, 30 Mar 2024 06:17:32 +0100

consult-el (0.35-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Fri, 20 Oct 2023 03:46:59 +0200

consult-el (0.32-1) unstable; urgency=medium

  [ Aymeric Agon-Rambosson ]
  * New upstream release.
  * Refresh patches.
  * d/control: Bump Standards-Version to 4.6.2 (no changes required).

  [ Lev Lamberov ]
  * Team upload.
  * Add gbp.conf to handle upstream version tags and pristine-tar.

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 07 Feb 2023 09:58:38 +0500

consult-el (0.20-1) unstable; urgency=medium

  * New upstream release.
  * Add explicit emacs recommends to elpa-consult.
  * Correct d/clean.
  * Refresh patches.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Sun, 16 Oct 2022 20:50:02 +0200

consult-el (0.19-2) unstable; urgency=medium

  * Source-only upload.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 07 Oct 2022 18:36:17 -0700

consult-el (0.19-1) unstable; urgency=medium

  * Initial release (Closes: #1016946).

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Tue, 04 Oct 2022 19:28:07 +0200
